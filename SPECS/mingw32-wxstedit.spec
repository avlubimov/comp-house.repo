%global __strip %{_mingw32_strip}
%global __objdump %{_mingw32_objdump}
%global _use_internal_dependency_generator 0
%global __find_requires %{_mingw32_findrequires}
%global __find_provides %{_mingw32_findprovides}
%define __debug_install_post %{_mingw32_debug_install_post}

%define pkgname wxstedit

Name:           mingw32-%{pkgname}
Version:        1.2.5
Release:        2%{?dist}
Summary:        wxStEdit Component

License:        wxWindows Library Licence, Version 3
Group:          Development/Libraries
URL:            http://wxcode.sourceforge.net/components/stedit/
Source0:        http://sourceforge.net/projects/wxcode/files/Components/wxStEdit/%{pkgname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-binutils
BuildRequires:  mingw32-wxWidgets-contrib
# Any additional BuildRequires.

%description
wxStEdit is a library and sample program for the wxWidgets's wxStyledTextCtrl
wrapper around the Scintilla text editor widget. It provides a number of
convenience functions and added capabilities, including the necessary
preferences, styles, language management, menu creation and updating, a
splitter, notebook, and frame component. Additionally it provides a find/
replace, editor settings, and property dialogs. It is designed to be easily
integrated into a larger program and while it tries to manage as much as
possible, it's fairly extensible as well. Individual features and "helper"
functionality can be turned off or overridden if desired.

%{?_mingw32_debug_package}


%prep
%setup -q -n %{pkgname}


%build

cp include/wx/stedit/setup0.h include/wx/stedit/setup.h
%{_mingw32_configure}  --with-wx-config=%{_mingw32_bindir}/wx-config

mingw32-make

%install
mingw32-make  DESTDIR=${RPM_BUILD_ROOT}   install

mkdir -p ${RPM_BUILD_ROOT}%{_mingw32_bindir}
mv    ${RPM_BUILD_ROOT}%{_mingw32_libdir}/wxcode_mswu_stedit-2.8.dll  ${RPM_BUILD_ROOT}%{_mingw32_bindir}
ln -s %{_mingw32_includedir}/wx/stedit/setup0.h      ${RPM_BUILD_ROOT}%{_mingw32_includedir}/wx/stedit/setup.h

#rm unnesessary man pages and static libraries
#rm -rf ${RPM_BUILD_ROOT}%{_mingw32_mandir}
#rm -rf ${RPM_BUILD_ROOT}%{_mingw32_libdir}


%files 
%doc docs/*
%{_mingw32_bindir}/wxcode_mswu_stedit-2.8.dll
%{_mingw32_libdir}/libwxcode_mswu_stedit-2.8.dll.a
%{_mingw32_includedir}/wx/stedit


%changelog
* Sun Oct 30 2011 Lubimov A.V. <avlubimov@gmail.com> - 5.1.4-1
- Initial RPM package



