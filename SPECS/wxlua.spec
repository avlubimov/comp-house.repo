%define pkgname wxLua

Name:           wxlua
Version:        2.8.10.0
Release:        1%{?dist}
Summary:        wxWidgets bindings for Lua

License:        wxWindows Library Licence, Version 3
Group:          Development/Libraries
URL:            http://wxlua.sourceforge.net/
Source0:        http://downloads.sourceforge.net/wxlua/%{pkgname}-%{version}-src.tar.gz

BuildRequires:  gcc lua-devel wxGTK-devel
BuildRequires:  lua wxGTK 

# Any additional BuildRequires.

%description
wxLua is a Lua scripting language wrapper around the wxWidgets cross-platform C++ GUI library..
It consists of two IDE type editors that can edit, debug, and run Lua programs (wxLua and wxLuaEdit),.
an executable for running standalone wxLua scripts (wxLuaFreeze),.
a Lua module that may be loaded using require("wx") when using the standard Lua executable,.
and a library for extending C++ programs with a fast, small, fully embeddable scripting language.


%prep
%setup -q -n %{pkgname}
perl -pi -e "s|lua5.1|lua|g" `find -name Makefile.in` configure

%build



%{configure}   --enable-systemlua

make CXXFLAGS="${CXXFLAGS} -fpermissive"

%install
make  DESTDIR=${RPM_BUILD_ROOT} install


(cd ${RPM_BUILD_ROOT}%{_includedir} && rm lauxlib.h  luaconf.h  lua.h  lualib.h)
rm -rf ${RPM_BUILD_ROOT}/usr/src

%files 
%doc docs 
%{_bindir}/*
%{_libdir}/*.so
%{_libdir}/lua/*/*.so
%{_libdir}/*.so.*

%{_includedir}/*
%{_datadir}/wxlua
%{_datadir}/applications/*
%{_datadir}/mime/*/*
%{_datadir}/pixmaps/*

%changelog
* Sun Oct 30 2011 Lubimov A.V. <avlubimov@gmail.com> - 5.1.4-1
- Initial RPM package



