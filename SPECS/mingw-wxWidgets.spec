%global __strip %{_mingw32_strip}
%global __objdump %{_mingw32_objdump}
%global _use_internal_dependency_generator 0
%global __find_requires %{_mingw32_findrequires}
%global __find_provides %{_mingw32_findprovides}
%define __debug_install_post %{_mingw32_debug_install_post}

%global mingw_pkg_name wxWidgets

Summary:       MinGW port of the wxWidgets GUI library
Name:          mingw-%{mingw_pkg_name}
Version:       2.8.12
Release:       5%{?dist}
License:       wxWidgets

Group:         Development/Libraries
URL:           http://wxwidgets.org
Source:        http://prdownloads.sourceforge.net/wxwindows/wxWidgets-%{version}.tar.gz
Patch0:        mingw-wxwidgets.patch
Patch1:        mingw-wxwidgets-png.patch
BuildArch:     noarch
BuildRequires: mingw32-gcc-c++
BuildRequires: mingw32-expat
BuildRequires: mingw32-libjpeg
BuildRequires: mingw32-libpng
BuildRequires: mingw32-libtiff
BuildRequires: mingw32-zlib
BuildRequires: gettext

%description
wxWidgets is the C++ cross-platform GUI library, offering classes for all
common GUI controls as well as a comprehensive set of helper classes for most
common application tasks, ranging from networking to HTML display and image
manipulation.

# Mingw32
%package -n mingw32-%{mingw_pkg_name}
Summary:		%{summary}

%description -n mingw32-%{mingw_pkg_name}
wxWidgets is the C++ cross-platform GUI library, offering classes for all
common GUI controls as well as a comprehensive set of helper classes for most
common application tasks, ranging from networking to HTML display and image
manipulation.

%package -n mingw32-%{mingw_pkg_name}-contrib
Summary: Contrib packages from %{mingw_pkg_name}

%description -n mingw32-%{mingw_pkg_name}-contrib
Contrib packages from %{mingw_pkg_name}

%package -n mingw32-%{mingw_pkg_name}-static
Summary:  Static libraries for mingw32-%{mingw_pkg_name} development
Group:    Development/Libraries
Requires: mingw32-%{mingw_pkg_name} = %{version}-%{release}

%description -n mingw32-%{mingw_pkg_name}-static
The mingw32-%{mingw_pkg_name}-static package contains static library for
mingw32-%{mingw_pkg_name} development.

%{?_mingw32_debug_package}

%prep
%setup -q -n wxWidgets-%{version}
%patch0 -p0 -b .mingw32
%patch1 -p0 -b .png14

#==========================================
%build
#========= Shared Libraries ==========
mkdir obj-shared
cd obj-shared
%_mingw32_configure --enable-shared \
  --with-msw \
  --with-sdl \
  --enable-unicode \
  --enable-optimise \
  --with-regex=builtin \
  --disable-rpath 
#  --without-subdirs


make %{?_smp_mflags}

cd contrib
make %{?_smp_mflags}
cd ..

cd ..


#========= Static Libraries ==========
mkdir obj-static
cd obj-static
%_mingw32_configure --disable-shared \
  --with-msw \
  --with-sdl \
  --enable-unicode \
  --enable-optimise \
  --with-regex=builtin \
  --disable-rpath 
#  --without-subdirs

#TODO verify this doesn't overwrite anything from the shared build
make %{?_smp_mflags}
cd ..

#========= Locale ====================
make -C locale allmo

#==========================================
%install
make install -C obj-shared DESTDIR=$RPM_BUILD_ROOT
make install -C obj-shared/contrib DESTDIR=$RPM_BUILD_ROOT
make install -C obj-static DESTDIR=$RPM_BUILD_ROOT


if ls $RPM_BUILD_ROOT%{_mingw32_libdir}/*.dll ; then
  mv $RPM_BUILD_ROOT%{_mingw32_libdir}/*.dll $RPM_BUILD_ROOT%{_mingw32_bindir}
else
  echo "No shared libraries found."
fi

# we need to modify the absolute wx-config link to be relative or rpm complains
# (and our package wouldn't be relocatable)
wx_config_filename=$(basename $RPM_BUILD_ROOT%{_mingw32_libdir}/wx/config/%{_mingw32_target}-*-release-[0-9]*)
ln -sf ../lib/wx/config/$wx_config_filename $RPM_BUILD_ROOT%{_mingw32_bindir}/wx-config

# remove bakefiles for now until we have a working bakefile setup for mingw32
rm -rf $RPM_BUILD_ROOT%{_mingw32_datadir}/bakefile

# find locale files
%find_lang wxstd
%find_lang wxmsw


%files -n mingw32-%{mingw_pkg_name}-contrib

%{_mingw32_bindir}/wxmsw28u_fl_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_gizmos_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_gizmos_xrc_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_ogl_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_plot_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_stc_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_svg_gcc_custom.dll
%{_mingw32_libdir}/libwx_mswu_fl-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_gizmos-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_gizmos_xrc-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_ogl-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_plot-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_stc-2.8-i686-pc-mingw32.dll.a
%{_mingw32_libdir}/libwx_mswu_svg-2.8-i686-pc-mingw32.dll.a


%files -n mingw32-%{mingw_pkg_name} -f wxstd.lang -f wxmsw.lang
%doc docs/licence.txt docs/licendoc.txt docs/lgpl.txt docs/gpl.txt
%{_mingw32_bindir}/wx-config
%{_mingw32_bindir}/wxrc-2.8
%{_mingw32_bindir}/wxrc.exe
%{_mingw32_bindir}/wxbase28u_gcc_custom.dll
%{_mingw32_bindir}/wxbase28u_net_gcc_custom.dll
%{_mingw32_bindir}/wxbase28u_xml_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_adv_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_aui_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_core_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_html_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_qa_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_richtext_gcc_custom.dll
%{_mingw32_bindir}/wxmsw28u_xrc_gcc_custom.dll

%{_mingw32_includedir}/wx-2.8
%{_mingw32_libdir}/libwx_baseu-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_baseu_net-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_baseu_xml-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_adv-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_aui-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_core-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_html-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_qa-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_richtext-2.8-%{_mingw32_target}.dll.a
%{_mingw32_libdir}/libwx_mswu_xrc-2.8-%{_mingw32_target}.dll.a
%dir %{_mingw32_libdir}/wx
%dir %{_mingw32_libdir}/wx/config
%{_mingw32_libdir}/wx/config/%{_mingw32_target}-msw-unicode-release-2.8
%dir %{_mingw32_libdir}/wx/include
%{_mingw32_libdir}/wx/include/%{_mingw32_target}-msw-unicode-release-2.8
%{_mingw32_datadir}/aclocal/wxwin.m4
#{_mingw32_datadir}/bakefile
#{_mingw32_datadir}/bakefile/presets
#{_mingw32_datadir}/bakefile/presets/wx.bkl
#{_mingw32_datadir}/bakefile/presets/wx_unix.bkl
#{_mingw32_datadir}/bakefile/presets/wx_win32.bkl

%files -n mingw32-%{mingw_pkg_name}-static
%{_mingw32_libdir}/libwx_baseu-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_baseu_net-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_baseu_xml-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_adv-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_aui-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_core-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_html-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_qa-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_richtext-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwx_mswu_xrc-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/libwxregexu-2.8-%{_mingw32_target}.a
%{_mingw32_libdir}/wx/config/%{_mingw32_target}-msw-unicode-release-static-2.8
%{_mingw32_libdir}/wx/include/%{_mingw32_target}-msw-unicode-release-static-2.8


%changelog
* Wed Jun 22 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.12-5
- reinstate italian mo file

* Sun Jun 19 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.12-4
- build mo files (reported by Fritz Elfert)

* Mon May 23 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.12-3
- transition to new package naming scheme

* Thu May  5 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.12-2
- include license file

* Wed May  4 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.12-1
- update to 2.8.12
- remove buildroot, defattr, clean

* Tue Apr 19 2011 Thomas Sailer <t.sailer@alumni.ethz.ch> - 2.8.11-1
- update to 2.8.11

* Tue Sep 8 2009 Michael Ansel <michael.ansel@gmail.com> - 2.8.10-1
- update to 2.8.10

* Tue Sep 8 2009 Michael Ansel <michael.ansel@gmail.com> - 2.8.9-3
- Adjust to Fedora packaging guidelines

* Wed Aug 26 2009 Michael Ansel <michael.ansel@gmail.com> - 2.8.9-2
- update for Fedora 11 (mingw -> mingw32)
- use mingw32 macros
- add static subpackage

* Thu Dec 18 2008 Keiichi Takahashi <bitwalk@users.soureforge.net> - 2.8.9-1
- update to 2.8.9

* Tue Aug 12 2008 Keiichi Takahashi <bitwalk@users.soureforge.net> - 2.8.8-1
- update to 2.8.8

* Sat Mar 15 2008 Keiichi Takahashi <bitwalk@users.soureforge.net> - 2.8.7-2
- rebuilt with current libraries.
- add BuildPrereq and Requires more explicitly.

* Thu Feb 28 2008 Keiichi Takahashi <bitwalk@users.soureforge.net> - 2.8.7-1
- initial release

