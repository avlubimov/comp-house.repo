%define pkgname wxstedit

Name:           %{pkgname}
Version:        1.2.5
Release:        2%{?dist}
Summary:        wxStEdit Component

License:        wxWindows Library Licence, Version 3
Group:          Development/Libraries
URL:            http://wxcode.sourceforge.net/components/stedit/
Source0:        http://sourceforge.net/projects/wxcode/files/Components/wxStEdit/%{pkgname}-%{version}.tar.gz


BuildRequires:  gcc wxGTK-devel lua-devel
Requires:  wxGTK lua


%description
wxStEdit is a library and sample program for the wxWidgets's wxStyledTextCtrl
wrapper around the Scintilla text editor widget. It provides a number of
convenience functions and added capabilities, including the necessary
preferences, styles, language management, menu creation and updating, a
splitter, notebook, and frame component. Additionally it provides a find/
replace, editor settings, and property dialogs. It is designed to be easily
integrated into a larger program and while it tries to manage as much as
possible, it's fairly extensible as well. Individual features and "helper"
functionality can be turned off or overridden if desired.

%prep
%setup -q -n %{pkgname}


%build

cp include/wx/stedit/setup0.h include/wx/stedit/setup.h
%{configure}  

make

%install
make  DESTDIR=${RPM_BUILD_ROOT}   install

rm -rf ${RPM_BUILD_ROOT}/%{_srcdir}

ln -s %{_includedir}/wx/stedit/setup0.h      ${RPM_BUILD_ROOT}%{_includedir}/wx/stedit/setup.h


%files 
%doc docs/*
%{_libdir}/*.so
%{_libdir}/*.so.*
%{_includedir}/wx/stedit


%changelog
* Sun Oct 30 2011 Lubimov A.V. <avlubimov@gmail.com> - 5.1.4-1
- Initial RPM package



