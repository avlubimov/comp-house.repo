Name:           cuneiform-linux
Version:        1.1.0
Release:        1%{?dist}
Summary:        Cuneiform for Linux
Group:          User Interface/Desktops
License:        BSD
URL:            https://launchpad.net/cuneiform-linux
Source0:        %{name}-%{version}.tar.bz2

ExclusiveArch:  i686 x86_64 

BuildRequires:  cmake
BuildRequires:  ImageMagick-c++-devel

Requires: ImageMagick-c++

%description
Cuneiform is an multi-language OCR system originally developed
and open sourced by Cognitive Technologies. Cuneiform was
originally a Windows program, which was ported to Linux
by Jussi Pakkanen..

%package devel
Summary:        Header files, libraries and development documentation for cuneiform
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description devel
This package contains the header files, static libraries and development
documentation for cuneiform. 

%prep
%setup -q

%build
mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=release -DCMAKE_INSTALL_PREFIX=%{_prefix} ..
make  %{?_smp_mflags}

cd ..

%install
mv "original russian readme.rtf" readme.rtf

cd build

make DESTDIR=%{buildroot}  install

%files 
%defattr(-,root,root,-)
%doc issues.txt readme.rtf readme.txt
%{_bindir}/*
%{_libdir}/*.so.*
%{_datadir}/*


%files devel
%defattr(-,root,root,-)
%{_includedir}/*
%{_libdir}/*.so

%changelog
* Thu Aug 25 2011 Hans de Goede <hdegoede@redhat.com> - 0.9.1-1
- initial rpm package 1.1.0

