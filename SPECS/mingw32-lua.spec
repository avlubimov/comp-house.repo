%global __strip %{_mingw32_strip}
%global __objdump %{_mingw32_objdump}
%global _use_internal_dependency_generator 0
%global __find_requires %{_mingw32_findrequires}
%global __find_provides %{_mingw32_findprovides}
%define __debug_install_post %{_mingw32_debug_install_post}

Name:           mingw32-lua
Version:        5.1.4
Release:        1%{?dist}
Summary:        Powerful light-weight programming language

License:        MIT
Group:          Development/Languages
URL:            http://www.lua.org/
Source0:        http://www.lua.org/ftp/lua-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-binutils
# Any additional BuildRequires.

%description
Lua is a powerful light-weight programming language designed for
extending applications. Lua is also frequently used as a
general-purpose, stand-alone language. Lua is free software.
Lua combines simple procedural syntax with powerful data description
constructs based on associative arrays and extensible semantics. Lua
is dynamically typed, interpreted from bytecodes, and has automatic
memory management with garbage collection, making it ideal for
configuration, scripting, and rapid prototyping.

%{?_mingw32_debug_package}


%prep
%setup -q -n lua-%{version}


%build
mingw32-make mingw CFLAGS="%{_mingw32_cflags} -D_WIN32" \
    CC="%{_mingw32_cc}" \
    MAKE="mingw32-make" \
    RANLIB="%{_mingw32_ranlib}" \
    INSTALL_TOP=%{_mingw32_prefix} \

%{_mingw32_strip} src/*.exe
%{_mingw32_strip} src/*.dll


%install
mingw32-make  TO_BIN="lua.exe luac.exe lua51.dll" \
    INSTALL_TOP=${RPM_BUILD_ROOT}%{_mingw32_prefix} \
    INSTALL_MAN=${RPM_BUILD_ROOT}%{_mingw32_mandir} \
    INSTALL_LMOD=${RPM_BUILD_ROOT}%{_mingw32_bindir}/lua \
    INSTALL_CMOD=${RPM_BUILD_ROOT}%{_mingw32_bindir} \
    install

#rm unnesessary man pages and static liblua.a
rm -rf ${RPM_BUILD_ROOT}%{_mingw32_mandir}
rm -rf ${RPM_BUILD_ROOT}%{_mingw32_libdir}


%files 
%doc COPYRIGHT HISTORY README  doc test etc/lua.ico
%{_mingw32_bindir}/lua51.dll
%{_mingw32_bindir}/lua.exe
%{_mingw32_bindir}/luac.exe
%{_mingw32_bindir}/lua
%{_mingw32_includedir}/*


%changelog
* Sun Oct 30 2011 Lubimov A.V. <avlubimov@gmail.com> - 5.1.4-1
- Initial RPM package



