%global __strip %{_mingw32_strip}
%global __objdump %{_mingw32_objdump}
%global _use_internal_dependency_generator 0
%global __find_requires %{_mingw32_findrequires}
%global __find_provides %{_mingw32_findprovides}
%define __debug_install_post %{_mingw32_debug_install_post}

%define pkgname wxLua
%define luadll  lua51

Name:           mingw32-%{pkgname}
Version:        2.8.10.0
Release:        1%{?dist}
Summary:        wxWidgets bindings for Lua

License:        wxWindows Library Licence, Version 3
Group:          Development/Libraries
URL:            http://wxlua.sourceforge.net/
Source0:        http://downloads.sourceforge.net/wxlua/%{pkgname}-%{version}-src.tar.gz
Patch:          mingw32-wxLua.wsock32.patch
BuildArch:      noarch

BuildRequires:  mingw32-filesystem
BuildRequires:  mingw32-gcc
BuildRequires:  mingw32-binutils
# Any additional BuildRequires.

%description
wxLua is a Lua scripting language wrapper around the wxWidgets cross-platform C++ GUI library..
It consists of two IDE type editors that can edit, debug, and run Lua programs (wxLua and wxLuaEdit),.
an executable for running standalone wxLua scripts (wxLuaFreeze),.
a Lua module that may be loaded using require("wx") when using the standard Lua executable,.
and a library for extending C++ programs with a fast, small, fully embeddable scripting language.


%{?_mingw32_debug_package}


%prep
%setup -q -n %{pkgname}

perl -pi -e "s|-llua5.1|-llua51|g" configure
perl -pi -e "s|lua_prefix/include/lua5.1|lua_prefix/include|g" configure
perl -pi -e "s|lua_prefix/lib|lua_prefix/bin|g" configure
perl -pi -e "s|lua5.1|lua51|g" `find -name Makefile.in`
%patch  -p 0
perl -pi -e 's|..\\\\..\\\\..\\\\art\\\\wxlua.ico|../../../art/wxlua.ico|g' `find apps -name "*.rc"`

%build



%{_mingw32_configure} --with-wx-config=%{_mingw32_bindir}/wx-config \
    --enable-systemlua --with-lua-prefix=%{_mingw32_prefix} \
    --libdir=%{_mingw32_bindir}

mingw32-make 

%install
mingw32-make  DESTDIR=${RPM_BUILD_ROOT} install

mkdir -p ${RPM_BUILD_ROOT}%{_mingw32_bindir}
mv ${RPM_BUILD_ROOT}%{_mingw32_libdir}/*.dll  ${RPM_BUILD_ROOT}%{_mingw32_bindir}
mv ${RPM_BUILD_ROOT}%{_mingw32_libdir}/lua/5.1/wx.dll  ${RPM_BUILD_ROOT}%{_mingw32_bindir}

#rm unnesessary man pages and static libraries
rm -rf ${RPM_BUILD_ROOT}%{_mingw32_datadir}

(cd ${RPM_BUILD_ROOT}%{_mingw32_includedir} && rm lauxlib.h  luaconf.h  lua.h  lualib.h)

rm -rf ${RPM_BUILD_ROOT}%{_mingw32_libdir}/lua


%files 
%doc docs samples
%{_mingw32_bindir}/wxluacan.exe  
%{_mingw32_bindir}/wxluaedit.exe
%{_mingw32_bindir}/wxlua.exe
%{_mingw32_bindir}/wx.dll
%{_mingw32_bindir}/wxluafreeze.exe
%{_mingw32_bindir}/wxlua_mswu_wxbindadv-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindaui-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindbase-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindcore-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindhtml-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindnet-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindrichtext-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindstc-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindxml-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxbindxrc-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxlua-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxluadebug-2.8.dll
%{_mingw32_bindir}/wxlua_mswu_wxluasocket-2.8.dll

%{_mingw32_libdir}/libwxlua_mswu_wxbindadv-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindaui-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindbase-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindcore-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindhtml-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindnet-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindrichtext-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindstc-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindxml-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxbindxrc-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxlua-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxluadebug-2.8.dll.a
%{_mingw32_libdir}/libwxlua_mswu_wxluasocket-2.8.dll.a

%{_mingw32_includedir}/*
%{_mingw32_prefix}/src/*

%changelog
* Sun Oct 30 2011 Lubimov A.V. <avlubimov@gmail.com> - 5.1.4-1
- Initial RPM package



