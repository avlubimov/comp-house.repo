# $Id$
# Authority: matthias

# ExclusiveDist: el4

Summary: Driver vloopback v4l2 for webcamstudio
Name: dkms-webcamstudio
Version: 1.0.2
Release: 1%{?dist}
License: GPLv2 or MPLv1.1
Group: System Environment/Kernel
URL: http://www.ws4gl.org/
Source: webcamstudio.tar.bz2
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root
BuildArch: noarch
Requires: gcc
Requires(post): dkms
Requires(preun): dkms

%description
vloopback device for webcamstudio

usage:
modprobe videodev
rmmod webcamstudio
modprobe webcamstudio devices=2


%prep
%setup -n webcamstudio

%build


%install
%{__rm} -rf %{buildroot}

%define dkms_name webcamstudio
%define dkms_vers %{version}-%{release}
%define quiet -q

# Kernel module sources install for dkms
%{__mkdir_p} %{buildroot}%{_usrsrc}/%{dkms_name}-%{dkms_vers}/
%{__cp} -a * %{buildroot}%{_usrsrc}/%{dkms_name}-%{dkms_vers}/

# Configuration for dkms
%{__cat} > %{buildroot}%{_usrsrc}/%{dkms_name}-%{dkms_vers}/dkms.conf << 'EOF'
PACKAGE_NAME=%{dkms_name}
PACKAGE_VERSION=%{dkms_vers}
MAKE[0]="make -C ${kernel_source_dir} M=${dkms_tree}/%{dkms_name}/%{dkms_vers}/build modules"
CLEAN[0]="make -C ${kernel_source_dir} M=${dkms_tree}/%{dkms_name}/%{dkms_vers}/build clean"
BUILT_MODULE_NAME[0]=webcamstudio
DEST_MODULE_LOCATION[0]=/extra
AUTOINSTALL="YES"
EOF


%clean
%{__rm} -rf %{buildroot}


%post
# Add to DKMS registry
dkms add -m %{dkms_name} -v %{dkms_vers} %{?quiet} || :
# Rebuild and make available for the currenty running kernel
dkms build -m %{dkms_name} -v %{dkms_vers} %{?quiet} || :
dkms install -m %{dkms_name} -v %{dkms_vers} %{?quiet} --force || :

%preun
# Remove all versions from DKMS registry
dkms remove -m %{dkms_name} -v %{dkms_vers} %{?quiet} --all || :


%files
%defattr(-,root,root,-)
%doc README
%{_usrsrc}/%{dkms_name}-%{dkms_vers}/


%changelog
